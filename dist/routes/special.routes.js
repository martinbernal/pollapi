"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const poll_controller_1 = require("../controllers/poll.controller");
const router = express_1.Router();
const authentication = passport_1.default.authenticate('jwt', { session: false });
router.get('/especial', authentication, ((req, res) => {
    let user = req.user;
    return res.json({ msg: `Hey ${user.email}!` });
}));
router.post('/polls/:id/vote', authentication, poll_controller_1.votePoll);
exports.default = router;
