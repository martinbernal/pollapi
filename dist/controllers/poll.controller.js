"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const poll_1 = __importDefault(require("../models/poll"));
exports.createPoll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body.question || !req.body.options) {
        return res.status(400).json({ msg: "required fields" });
    }
    let options = req.body.options;
    req.body.options = [];
    options.forEach((value, index) => {
        req.body.options.push({ name: value, votes: [] });
    });
    const newPoll = new poll_1.default(req.body);
    yield newPoll.save();
    return res.status(201).json(newPoll);
});
exports.getPolls = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const pollsA = yield poll_1.default.aggregate([
        { "$project": {
                "_id": 1,
                "question": 1,
                "options": {
                    "$map": {
                        "input": "$options",
                        "as": "option",
                        "in": {
                            "_id": "$$option._id",
                            "votes_count": { "$size": "$$option.votes" },
                            "name": "$$option.name",
                            "votes": "$$option.votes"
                        }
                    }
                }
            }
        }
    ]);
    return res.status(200).json(pollsA);
});
exports.getPoll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.params.id)
        return res.status(400).json({ msg: "Must provide the ID of the poll" });
    const poll = yield poll_1.default.findById(req.params.id, ((err) => {
        if (err)
            return res.status(404).json({ msg: "Error" });
    }));
    if (!poll) {
        return res.status(404).json({ msg: "Poll content not found" });
    }
    return res.status(200).json(poll);
});
exports.deletePoll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.params.id)
        return res.status(400).json({ msg: "Must provide the ID of the poll" });
    const poll = yield poll_1.default.findById(req.params.id, ((err) => {
        if (err)
            return res.status(404).json({ msg: "Error" });
    }));
    if (!poll) {
        return res.status(404).json({ msg: "Poll content not found" });
    }
    const deleted = yield poll_1.default.findByIdAndDelete(req.params.id);
    if (!deleted)
        return res.status(404).json({ msg: "Error deleting the poll." });
    return res.status(204).send();
});
exports.votePoll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let user = req.user;
    if (!req.params.id)
        return res.status(400).json({ msg: "Must provide the ID of the poll" });
    if (!req.body.voted_option)
        return res.status(400).json({ msg: "Must provide the ID of the selected option" });
    const poll = yield poll_1.default.findById(req.params.id, ((err) => {
        if (err)
            return res.status(404).json({ msg: "Error" });
    }));
    if (!poll) {
        return res.status(404).json({ msg: "Poll content not found" });
    }
    const option = yield poll_1.default.findOne({ "_id": req.params.id, "options._id": req.body.voted_option }, (err) => {
        if (err)
            return res.status(404).json({ msg: "The data provided is not valid" });
    });
    if (!option)
        return res.status(400).json({ msg: "The option doesn't match with the provided poll" });
    const voted = yield poll_1.default.findOne({ "_id": req.params.id, "options.votes.user": user.id }, (err) => {
        if (err)
            return res.status(404).json({ msg: "The data provided is not valid" });
    });
    if (voted)
        return res.status(400).json({ msg: "You already voted for this poll" });
    const vote = { user: user.id };
    const testVote = yield poll_1.default.updateOne({ _id: req.params.id }, { $push: { "options.$[inner].votes": vote } }, { arrayFilters: [{ "inner._id": req.body.voted_option }] });
    return res.status(200).json({ success: true });
});
