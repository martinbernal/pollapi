"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const vote_1 = __importDefault(require("./vote"));
const OptionSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    votes: [vote_1.default]
});
exports.default = OptionSchema;
