"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const option_1 = __importDefault(require("./option"));
const pollSchema = new mongoose_1.Schema({
    question: {
        type: String,
        required: true,
        trim: true
    },
    options: [option_1.default]
});
exports.default = mongoose_1.model('Poll', pollSchema);
