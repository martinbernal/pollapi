import express from 'express';
import morgan from  'morgan';
import cors from 'cors';
import publicRoutes from './routes/public.routes';
import authRoutes from './routes/auth.routes';
import passport from "passport";
import passportMiddleware from "./middlewares/passport";
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUi, {SwaggerOptions} from 'swagger-ui-express';
import swaggerDoc from './swagger.json';

const app  = express();

app.set('port', process.env.PORT || 3000);

app.use(morgan('dev'));
app.use(cors())
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(passport.initialize());
passport.use(passportMiddleware);

app.get('/', (req, res)=>{
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    res.send(`<a href="/doc">Api doc at ${fullUrl + "doc"}</a>`)
});


app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.use(publicRoutes);
app.use(authRoutes);

export default app;