import mongoose from 'mongoose';
import config from './config/config';

mongoose.connect(config.DB.URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    user: config.DB.USER,
    pass: config.DB.PASSWORD
});

const connection = mongoose.connection;

connection.once('open', ()=>{
    console.log("Mongodb connection stablished");
});

connection.on('error', err=>{
   console.log(err);
   process.exit(0);
});