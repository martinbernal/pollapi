import {Router} from 'express';
import passport from "passport";
import {IUser} from "../models/user";
import {createPoll, getPoll, votePoll, getPolls, deletePoll} from "../controllers/poll.controller";

const router = Router();

const authentication =  passport.authenticate('jwt', {session: false});

router.post('/polls',  authentication, createPoll);
router.get('/polls', authentication, getPolls);
router.get('/polls/:id', authentication, getPoll);
router.delete('/polls/:id', authentication, deletePoll);
router.post('/polls/:id/vote', authentication, votePoll);

export default router;
