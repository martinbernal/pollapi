import {Request, Response} from 'express';
import Poll, {IPoll} from "../models/poll";
import Vote, {IVote} from "../models/vote";
import mongoose from 'mongoose';

export const createPoll = async (req: Request, res: Response): Promise<Response> => {
    if (!req.body.question || !req.body.options) {
        return res.status(400).json({msg: "required fields"});
    }

    let options =req.body.options;
    req.body.options=[];
    options.forEach((value: any, index:number)=>{
        req.body.options.push({name: value, votes:[]});
    });

    const newPoll= new Poll(req.body);
    await newPoll.save();
    return res.status(201).json(newPoll);
};


export const getPolls = async (req: Request, res: Response): Promise<Response> =>{

    const pollsA = await Poll.aggregate([
        {"$project": {
                "_id":1,
                "question":1,
                "total_votes": {"$sum" : {
                        "$map":{
                            "input": "$options",
                            "as": "option",
                            "in": {"$size": "$$option.votes"}
                        }
                    }},
                "options":{
                    "$map":{
                        "input": "$options",
                        "as": "option",
                        "in": {
                            "_id": "$$option._id",
                            "option_votes": {"$size": "$$option.votes"},
                            "name": "$$option.name",
                            "votes": "$$option.votes"
                        }
                    }
                }
            }
        }]
    );
    return res.status(200).json(pollsA);
};

export const getPoll = async (req: Request, res: Response): Promise<Response> =>{
    if(!req.params.id) return res.status(400).json({msg: "Must provide the ID of the poll"});
    const poll= await Poll.findById(req.params.id, ((err :any) => {
        if(err) return res.status(404).json({msg: "Error"});

    }));

    if(!poll){
        return res.status(404).json({msg: "Poll content not found"});
    }

    return res.status(200).json(poll);
};

export const deletePoll = async (req: Request, res: Response): Promise<Response> =>{
    if(!req.params.id) return res.status(400).json({msg: "Must provide the ID of the poll"});
    const poll= await Poll.findById(req.params.id, ((err :any) => {
        if(err) return res.status(404).json({msg: "Error"});

    }));

    if(!poll){
        return res.status(404).json({msg: "Poll content not found"});
    }

    const deleted= await Poll.findByIdAndDelete(req.params.id);

    if(!deleted) return res.status(404).json({msg: "Error deleting the poll."});

    return res.status(204).send();
};


export const votePoll = async (req : Request, res: Response): Promise<Response> => {
    let user : any = req.user;
    if(!req.params.id) return res.status(400).json({msg: "Must provide the ID of the poll"});
    if(!req.body.voted_option) return res.status(400).json({msg: "Must provide the ID of the selected option"});

    const poll= await Poll.findById(req.params.id, ((err :any) => {
        if(err) return res.status(404).json({msg: "Error"});

    }));

    if(!poll){
        return res.status(404).json({msg: "Poll content not found"});
    }

    const option = await Poll.findOne({"_id": req.params.id, "options._id":req.body.voted_option}, (err: any)=>{
        if(err) return res.status(404).json({msg: "The data provided is not valid"});
    });

    if(!option) return res.status(400).json({msg: "The option doesn't match with the provided poll"});



    const voted= await Poll.findOne({"_id": req.params.id, "options.votes.user": user.id}, (err: any)=>{
        if(err) return res.status(404).json({msg: "The data provided is not valid"});
    });

    if(voted) return res.status(400).json({msg: "You already voted for this poll"});

    const vote = { user: user.id};

    const testVote = await Poll.updateOne(
        { _id: req.params.id},
        {$push:{"options.$[inner].votes": vote}},
        {arrayFilters: [{"inner._id": req.body.voted_option}]}
    );


    return res.status(200).json({success: true});
}
