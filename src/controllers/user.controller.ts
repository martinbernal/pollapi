import {Request, Response} from 'express';
import User, {IUser} from "../models/user";
import jwt from 'jsonwebtoken';
import config from '../config/config';

function createToken(user: IUser) {
    return jwt.sign({id: user.id, email: user.email}, config.jwtSecret, {
        expiresIn: 86400
    })
}

export const singUp = async (req: Request, res: Response): Promise<Response> => {
    if (!req.body.email || !req.body.password) {
        return res.status(400).json({msg: "Email and Password required"});
    }

    const user = await User.findOne({email: req.body.email});
    if(user){
        return res.status(400).json({msg: 'User already exists'});
    }

    const newUser= new User(req.body);
    await newUser.save();
    return res.status(201).json(newUser);
}

export const singIn = async (req: Request, res: Response): Promise<Response> => {
    if (!req.body.email || !req.body.password) {
        return res.status(400).json({msg: "Email and Password required"});
    }

    const user = await User.findOne({email: req.body.email});

    if(!user){
        return res.status(404).json({msg: 'User not exist'});
    }

    if(await  user.comparePassword(req.body.password)){
        return res.status(200).json({token: createToken(user)});
    }

    return res.status(400).json({msg: "The email and password combination doesn't exists."})
}