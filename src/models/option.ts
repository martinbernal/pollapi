import {model, Schema, Document} from 'mongoose';
import voteSchema from "./vote";

export interface IOption extends Document{
    name: string;
}

const OptionSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    votes : [voteSchema]

});


export default OptionSchema;
