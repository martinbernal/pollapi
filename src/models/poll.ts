import {model, Schema, Document} from 'mongoose';
import OptionSchema, {IOption} from "./option";

export interface IPoll extends Document{
    question: string,
    options: [IOption]
}

const pollSchema = new Schema({
    question: {
        type: String,
        required: true,
        trim: true
    },
    options: [OptionSchema]
});

export default model<IPoll>('Poll', pollSchema);