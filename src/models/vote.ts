import {model, Schema, Document} from 'mongoose';
import * as mongoose from "mongoose";
export interface IVote extends Document{
    user: string;
    created: Date;
}

const voteSchema = new Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    created: { type: Date, default: Date.now }
});

export default voteSchema;
