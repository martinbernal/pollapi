export default {
    jwtSecret: process.env.JWT_SECRET || 'pollsapp-secret',
    DB:{
        URI: process.env.MONGODB_URI || 'mongodb://localhost:32770/pollsapp',
        USER: process.env.MONGODB_USER || '',
        PASSWORD: process.env.MONGODB_PASSWORD || ''
    }
}
